# Problem Statement - Assignment | Week 3

Create a Class Pet. Each instance of the class will be one electronic pet for the user to take care of. Each instance will have a current state, consisting of three Instance variables:

    Hunger - integer

    Boredom -  an integer

    Sounds - a list of strings, each a word that the pet has been taught to say

Methods

* In the __init__ method of the class, hunger and boredom are initialized to random values between 0 and the threshold for being hungry or bored. Initialize the sounds instance variable appropriately so that teaching a sound to any of the pets would not teach it to all instances of the class!

* There is a clock_tick method which just increments the boredom and hunger instance variables, simulating the idea that as time passes, the pet gets more bored and hungry.

* The __str__ method produces a string representation of the pet’s current state, notably whether it is bored or hungry or whether it is happy. It’s bored if the boredom instance variable is larger than the threshold, which is set as a class variable.

* To relieve boredom, the pet owner can either teach the pet a new word, using the teach() method, or interact with the pet, using the hi() method. In response to teach(), the pet adds the new word to its list of words. In response to the hi() method, it prints out one of the words it knows, randomly picking one from its list of known words. Both hi() and teach() cause an invocation of the reduce_boredom() method. It decrements the boredom state by an amount that it reads from the class variable boredom_decrement. The boredom state can never go below 0.

* To relieve hunger, we call the feed() method which will cause an invocation of the reduce_hunger() method.

Features

* Interact with the user. At each iteration, display a text prompt reminding the user of what commands are available.

* The user will have a list of pets, each with a name. The user can issue a command to adopt a new pet, which will create a new instance of Pet. Or the user can interact with an existing pet, with a Greet, Teach, or Feed command.

* Teach new words to pets so that each pet knows a different set of words.

* No matter what the user does, with each command entered, the clock ticks for all their p

# Problem Statement - Assignment | Week 2

Create a Tic Tac Toe game.

Here are the requirements:

    2 players should be able to play the game (user, computer)
    The board should be printed out every time a player makes a move
    You should be able to accept input of the player position and then place a symbol on the board

- Store the Game logs (info about the moves, result) for 10 games into a data structure.
- You should be able to recreate the moves and print out the board for each of the moves for any of the 10 games when the user inputs the game number.
For example - if the user says that he wants to see how Game 3 was played, you should print out all the moves on the board and print out the winner at the end of the game.

# Problem Statement - Assignment | Week 1

Make a two-player Rock-Paper-Scissors game. One of the players is the computer.

10 rounds. Print out the winner and points earned by both players at the end of the game.

Remember the rules:

Rock beats Scissors
Scissors beats Paper
Paper beats Rock

Use a dictionary to store the history of choices made by the Player and the Computer.

You should be able to print the choices made by the Player and the Computer in each round once the game is completed.

Ex -

Enter the round for which you need the information >> 3
Output

Player choice = Rock

Computer choice = Paper

Player won Round 3
