# A program to spend time with pets
from random import randint


class Pet:
    name = ''
    hunger = 0
    boredom = 0
    boredom_decrement = 5
    hunger_decrement = 5
    sounds = []
    animal_type = ''

    def clock_tick(self):
        self.hunger += 1
        self.boredom += 1

    def __init__(self, animal_type, name):
        self.name = name
        self.hunger = randint(0, 5)
        self.boredom = randint(0, 5)
        self.animal_type = animal_type

        if self.animal_type == 'Dog':
            self.sounds.append('Woof')
        elif self.animal_type == 'Cat':
            self.sounds.append('Meow')
        elif self.animal_type == 'Bird':
            self.sounds.append('Chirp')
        else:
            self.sounds.append('')

    def reduce_hunger(self):
        if self.hunger >= 5:
            self.hunger -= self.hunger_decrement
        else:
            self.hunger = 0

    def reduce_boredom(self):
        if self.boredom >= 5:
            self.boredom -= self.boredom_decrement
        else:
            self.boredom = 0

    def teach(self):
        word = input(f'What would you like to teach {self.name} ? ')
        self.sounds.append(word)
        print(f'Yay! {self.name} has learned {word}')
        self.reduce_boredom()

    def hi(self):
        random_word = randint(0, len(self.sounds)-1)
        print(f'{self.name} says {self.sounds[random_word]} {self.sounds[random_word]} !')
        self.reduce_boredom()

    def feed(self):
        print(f'Yay! {self.name} is going to be feeded!')
        food = input(f'What would you like to feed {self.name} ?')
        self.reduce_hunger()

    def __str__(self):
        if self.boredom >= 5:
            bore = 'Bored'
        else:
            bore = 'Happy'
        if self.hunger >= 5:
            hungry = 'Hungry'
        else:
            hungry = 'Not hungry'
        return f'{self.name} is {bore} and {hungry}'


# Main program starts
print('#############################')
print(' WELCOME TO INAPP PET SHOP')
print('#############################')
choice = 1
pets = []
while choice != 6:
    print('Please choose an option from the menu below:')
    print('1.Adopt a pet\t 2.Teach a pet\t 3.Say hi\t 4.Feed a pet\t 5.State of pet\t6.Exit')
    if len(pets) != 0:
        print('List of pets:')
        for pet_ in pets:
            print(f'{pet_.name}', end=' ')
        print('\n')
    choice = int(input('Choice (1/2/3/4/5/6) : '))
    if choice == 1:
        names = input('Enter the name of the pet : ')
        animal = input(f'Enter the type of {names}: ')
        pet = Pet(animal, names)
        print(f'Yay! {names}, the electronic pet has been created ')
        pets.append(pet)
    else:
        if choice == 6:
            break
        names = input('Enter the name of the pet : ')
        for pet_search in pets:
            if pet_search.name == names:
                print(f'{names} has been found!')
                if choice == 2:
                    pet_search.teach()
                elif choice == 3:
                    pet_search.hi()
                elif choice == 4:
                    pet_search.feed()
                elif choice == 5:
                    print(pet_search)
                else:
                    print('Exiting...')
                    choice = 6
    for pet_search in pets:
        pet_search.clock_tick()