import sqlite3


# CREATE TABLE

def db_connect():
    conn = sqlite3.connect('hospitals.db')
    return conn


def add_to_hospital():
    con = db_connect()
    cur = con.cursor()
    hospital_sql = """
    INSERT INTO Hospital (Hospital_ID,Hospital_name,Bed_Count) VALUES (?,?,?)
    """
    cur.execute(hospital_sql, (1, 'Mayo Clinic', 200))
    cur.execute(hospital_sql, (2, 'Cleveland Clinic', 400))
    cur.execute(hospital_sql, (3, 'Johns Hopkins', 1000))
    cur.execute(hospital_sql, (4, 'UCLA Medical Center', 1500))
    con.commit()
    con.close()


def add_to_doctor():
    con = db_connect()
    cur = con.cursor()
    doc_sql = """
    INSERT INTO Doctor (Doctor_ID,Doctor_name,Hosp_ID,Joining_Date,Speciality,Salary) VALUES (?,?,?,?,?,?)
    """
    cur.execute(doc_sql, (101, 'David', 1, '2005-02-10', 'Pediatric', 40000))
    cur.execute(doc_sql, (102, 'Michael', 1, '2018-07-23', 'Oncologist', 20000))
    cur.execute(doc_sql, (103, 'Susan', 2, '2016-05-19', 'Garnacologist', 25000))
    cur.execute(doc_sql, (104, 'Robert', 2, '2017-12-28', 'Pediatric', 28000))
    cur.execute(doc_sql, (105, 'Linda', 3, '2004-06-04', 'Garnacologist', 42000))
    cur.execute(doc_sql, (106, 'William', 3, '2012-09-11', 'Dermatologist', 30000))
    cur.execute(doc_sql, (107, 'Richard', 4, '2014-08-21', 'Garnacologist', 32000))
    cur.execute(doc_sql, (108, 'Karen', 4, '2011-10-17', 'Radiologist', 30000))
    con.commit()
    con.close()


def create_tables():
    con = db_connect()
    cur = con.cursor()
    hospital_sql = """
    CREATE TABLE Hospital(
    Hospital_ID integer PRIMARY KEY,
    Hospital_name text NOT NULL,
    Bed_Count integer NOT NULL)"""

    cur.execute(hospital_sql)

    doctor_sql = """
    CREATE TABLE Doctor(
    Doctor_ID integer PRIMARY KEY,
    Doctor_name text NOT NULL,
    Hosp_ID integer NOT NULL,
    Joining_Date text NOT NULL,
    Speciality text NOT NULL,
    Salary integer NOT NULL,
    Experience integer NULL,
    FOREIGN KEY (Hosp_ID) REFERENCES Hospital (Hospital_ID))
    """

    cur.execute(doctor_sql)
    con.commit()
    con.close()


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        create_tables()
        add_to_hospital()
        add_to_doctor()
        con = db_connect()
        cur = con.cursor()
        speciality = input('Enter the speciality: ')
        salary = int(input('Enter the salary: '))
        cur.execute(
            "SELECT Doctor_ID,Doctor_name,Speciality,Salary FROM Doctor WHERE Speciality = '{}' AND Salary > '{}' ".format(
                speciality, salary))
        # cur.execute('SELECT* FROM Doctor')
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        print('Moving onto Question (c)....')
        id = int(input('Enter the Hospital ID: '))
        cur.execute(
            "SELECT Doctor_name,Hospital_name FROM Hospital JOIN Doctor ON Hosp_ID = Hospital_ID WHERE Hospital_ID='{}'".format(
                id))
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()

    except:
        raise RuntimeError("Uh oh, an error occurred ...")
    finally:
        print('Finally executed')
