import sqlite3
# CREATE TABLE

# Connect to sqlite database
conn = sqlite3.connect('cars.db')
# Create cursor object
cur = conn.cursor()
# cur.execute("DROP TABLE IF EXISTS cars")
cars_sql = """
CREATE TABLE cars(
car_name text NOT NULL,
car_owner text NOT NULL)
"""
cur.execute(cars_sql)
conn.commit()

# INSERT 10 RECORDS INTO TABLE
insert_query = "INSERT INTO cars (car_name,car_owner) VALUES (?,?)"
cur.execute(insert_query, ('Lambhorgini', 'Mahesh'))
cur.execute(insert_query, ('Porsche', 'Surya'))
cur.execute(insert_query, ('Toyota', 'Arya'))
cur.execute(insert_query, ('Honda', 'Alex'))
cur.execute(insert_query, ('Mercedes', 'Natlie'))
cur.execute(insert_query, ('Tesla', 'Charlie'))
cur.execute(insert_query, ('Hyundai', 'Maria'))
cur.execute(insert_query, ('Volkswagan', 'Nick'))
cur.execute(insert_query, ('Maruthi', 'George'))
cur.execute(insert_query, ('Jaguar', 'Ria'))

conn.commit()
# DISPLAY DATA FROM TABLE
cur.execute("SELECT * FROM cars")
for row in cur:
    print(f'{row}')
conn.close()


