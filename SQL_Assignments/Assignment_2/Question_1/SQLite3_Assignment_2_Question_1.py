import sqlite3


# CREATE TABLE

def db_connect():
    conn = sqlite3.connect('employees.db')
    return conn


def create_table():
    con = db_connect()
    cur = con.cursor()
    employee_sql = """
    CREATE TABLE Employee(
    ID integer PRIMARY KEY,
    Name text NOT NULL,
    Salary integer NOT NULL,
    Department_ID integer NOT NULL)"""

    cur.execute(employee_sql)

    add_column_sql = """
    ALTER TABLE Employee ADD COLUMN City text NOT NULL
    """
    cur.execute(add_column_sql)

    con.commit()
    con.close()


def add_employees():
    con = db_connect()
    cur = con.cursor()
    employee_sql = """
    INSERT INTO Employee (Name,ID,Salary,Department_ID,City) VALUES (?,?,?,?,?)
    """
    cur.execute(employee_sql, ('David', 101, 35000, 1, 'Trivandrum'))
    cur.execute(employee_sql, ('Lily', 102, 25000, 2, 'Kozhikode'))
    cur.execute(employee_sql, ('Natasha', 103, 30000, 1, 'Palakkad'))
    cur.execute(employee_sql, ('Mary', 104, 40000, 3, 'Kozhikode'))
    cur.execute(employee_sql, ('Percy', 105, 35000, 2, 'Trivandrum'))

    con.commit()
    con.close()


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        create_table()
        add_employees()
        con = db_connect()
        cur = con.cursor()
        cur.execute("SELECT Name,ID,Salary FROM Employee")
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        print('Moving onto question (e)..')
        character = input('Enter the character to find a name that starts with it: ')
        cur.execute("SELECT Name,ID,City FROM Employee WHERE Name LIKE '{}%'".format(character))
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        print('Moving onto question (f)..')
        id = int(input('Enter the ID to find the details: '))
        cur.execute("SELECT Name,ID,City,Department_ID FROM Employee WHERE ID = '{}'".format(id))
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        print('Moving onto question (g)..')
        id = int(input('Enter the ID to find the details: '))
        name = input("Enter the new name : ")
        cur.execute("UPDATE Employee SET Name = '{}' WHERE ID = '{}'".format(name, id))
        con.commit()
        cur = con.cursor()
        print('Updated DB:')
        cur.execute("SELECT * FROM Employee")
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()

    except:
        print('Oh!..an error has happened')
