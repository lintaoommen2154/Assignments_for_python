import sqlite3


# CREATE TABLE

def db_connect():
    conn = sqlite3.connect('employees.db')
    return conn


def create_table():
    con = db_connect()
    cur = con.cursor()
    employee_sql = """
    CREATE TABLE Departments(
    Dept_ID integer PRIMARY KEY,
    Department_Name text NOT NULL,
    FOREIGN KEY (Dept_ID) REFERENCES Employee (Department_ID))
    """

    cur.execute(employee_sql)

    con.commit()
    con.close()


def add_department_details():
    con = db_connect()
    cur = con.cursor()
    dept_sql = """
    INSERT INTO Departments (Dept_ID,Department_Name) VALUES (?,?)
    """
    cur.execute(dept_sql, (1, 'Research and Development'))
    cur.execute(dept_sql, (2, 'IT'))
    cur.execute(dept_sql, (3, 'HR'))
    cur.execute(dept_sql, (4, 'Accounting and Finance'))
    cur.execute(dept_sql, (5, 'Production'))

    con.commit()
    con.close()


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        create_table()
        add_department_details()
        con = db_connect()
        cur = con.cursor()
        id = int(input('Enter the Department ID to find the details: '))
        cur.execute("SELECT Name,ID,City,Department_ID,Department_Name FROM Employee JOIN Departments ON Dept_ID = Department_ID WHERE Department_ID = '{}'".format(id))
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()

    except:
        print('Oh!..an error has happened')
