import sqlite3


def db_connect():
    conn = sqlite3.connect('database.sqlite')
    return conn


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        con = db_connect()
        cur = con.cursor()
        cur.execute("SELECT * FROM Teams") # Question 2)a
        print(f'No of rows :{len(cur.fetchall())}')
        cur.execute("SELECT DISTINCT Season FROM Teams")  # Question 2)b
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT MIN(StadiumCapacity),MAX(StadiumCapacity) FROM Teams")  # Question 2)c
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT SUM(KaderHome) FROM Teams WHERE Season = '2014'")  # Question 2)d
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT AVG(FTHG) FROM Matches WHERE HomeTeam = 'Man United'")  # Question 2)e
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()
    except:
        print('Sorry! An error has occured')
