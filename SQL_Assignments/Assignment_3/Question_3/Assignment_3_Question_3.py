import sqlite3


def db_connect():
    conn = sqlite3.connect('database.sqlite')
    return conn


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        con = db_connect()
        cur = con.cursor()
        cur.execute(
            "SELECT HomeTeam,FTHG,FTAG,Season FROM Matches WHERE Season= 2010 AND HomeTeam = 'Aachen' ORDER BY FTHG DESC ")  # Question 3)a
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT COUNT(FTR) AS GameCount,HomeTeam FROM Matches WHERE Season = 2016 AND FTR='H' GROUP BY HomeTeam ORDER BY GameCount DESC")  # Question 3)b
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT * FROM Unique_Teams LIMIT 10")  # Question 3)c
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT M.Match_ID,U.Unique_Team_ID,U.TeamName FROM Unique_Teams U,Teams_in_Matches M WHERE U.Unique_Team_ID = M.Unique_Team_ID") # Question 3)d using WHERE
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT Match_ID,Unique_Teams.Unique_Team_ID,TeamName FROM Unique_Teams JOIN Teams_in_Matches ON Unique_Teams.Unique_Team_ID = Teams_in_Matches.Unique_Team_ID") # Question 3)d using JOIN
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT Unique_Team_ID,Unique_Teams.TeamName,Season,ForeignPlayersHome,StadiumCapacity FROM Teams JOIN Unique_Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 10") # Question 3)e
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT Unique_Team_ID,Unique_Teams.TeamName,AvgAgeHome,Season,ForeignPlayersHome FROM Unique_Teams JOIN Teams ON Unique_Teams.TeamName = Teams.TeamName LIMIT 5")  # Question 3)f
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT MAX(Match_ID),Unique_Teams.Unique_Team_ID,TeamName FROM Teams_in_Matches JOIN Unique_Teams ON Unique_Teams.Unique_Team_ID=Teams_in_Matches.Unique_Team_ID WHERE TeamName LIKE '%r' OR TeamName LIKE '%y' GROUP BY TeamName")  # Question 3)g
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()
    except:
        print('Sorry! An error has occured')
