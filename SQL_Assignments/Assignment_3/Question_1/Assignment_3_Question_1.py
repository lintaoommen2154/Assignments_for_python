import sqlite3


def db_connect():
    conn = sqlite3.connect('database.sqlite')
    return conn


# MAIN FUNCTION
if __name__ == "__main__":
    try:
        con = db_connect()
        cur = con.cursor()
        cur.execute("SELECT HomeTeam,AwayTeam FROM Matches WHERE Season = 2015 AND FTHG=5")  # Question 1)b
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute(
            "SELECT Match_ID,Div,Season,Date,AwayTeam,FTR FROM Matches WHERE AwayTeam = 'Arsenal' AND FTR='A'")  # Question 1)c
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute("SELECT Match_ID,Div,Season,Date,AwayTeam FROM Matches WHERE AwayTeam = 'Bayern Munich' AND FTHG >2 AND Season BETWEEN '2012' AND '2015'") # Question 1)d
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        cur.execute(
            "SELECT Match_ID,Div,Season,Date,HomeTeam,AwayTeam FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%'") # Question 1)e
        row = cur.fetchone()
        while row is not None:
            print(row)
            row = cur.fetchone()
        con.close()
    except:
        print('Sorry! An error has occured')
