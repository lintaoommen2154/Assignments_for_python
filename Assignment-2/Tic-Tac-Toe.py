from random import randint

winning_spots = [('1', '5', '9'), ('3', '5', '7'), ('2', '5', '8'), ('1', '4', '7'), ('3', '6', '9'), ('1', '2', '3'),
                 ('4', '5', '6'), ('7', '8', '9')]  # The spots where 3 in a line indicate a win
position_dict, game_history = {}, {}  # position_dict : To store the moves in a round, game_history: To store the moves in every round

def flush():
    for i in range(1, 10):
        position_dict[str(i)] = ' '  # To clear the board after one round

def display_board(moves_list=None):  # To display the board
    print("\nThe positions are 1 from top right filled from left to right")
    for i in range(1, 10):
        if i == 4 or i == 7:
            print('\n--|---|---|')
        if moves_list is not None:
            print(f'{moves_list[i - 1]} |\t', end="")  # This display is used only when the history of a round displayed
        else:
            print(f'{position_dict[str(i)]} |\t', end="")

def check_if_game_ends(count_of_game):  # TO check if game ends
    if ' ' not in position_dict.values():  # Checks if there is any spaces
        print('\nNo one wins')
        game_history[str(count_of_game)] = list(position_dict.values())  # Storing the game history
        return True
    for a, b, c in winning_spots:  # Checks if player or computer won the game
        if position_dict[a] == position_dict[b] == position_dict[c] and position_dict[a] != ' ':
            print(f'\n{position_dict[a]} wins the game')
            game_history[str(count_of_game)] = list(position_dict.values())  # Storing the game history
            return True
    return False

def play_game(game_count):
    player_choice = input("Does Player 1 choose X or O (insert X or O) : ")
    comp_choice = 'O' if player_choice == 'X' else 'X'   # Choose the computer choice accordingly
    while (True):
        display_board()
        position = input(f'\nEnter the position to enter the {player_choice} ')
        position_dict[position] = player_choice  # Storing player position in the board
        if check_if_game_ends(game_count):
            flush()
            break
        comp_pos = str(randint(1, 9))  # Randomly the computer chooses where to place provided it is empty
        while position_dict[comp_pos] != ' ':
            comp_pos = str(randint(1, 10))
        position_dict[comp_pos] = comp_choice  # Storing computer position in the board

keep_playing = True  # Start the game
game_count = 1 # Initially the round is 1
while keep_playing:
    play_game(game_count)
    continue_game = input('Wish to play another round ? (Y/N)')
    game_count += 1
    keep_playing = True if continue_game == 'Y' else False
check_game = input('Enter the round to print the board')  # To print the history of a round
list_of_moves = game_history[check_game]
display_board(list_of_moves)
for i in range(1, 10):
    position_dict[str(i)] = list_of_moves[i - 1]   # The board is restored to the moves in the required round and the winner is checked
ending = check_if_game_ends(check_game)
