# Rock-Paper-Scissors Game ( Total lines of code : 31)
# This is a simple python game implemented using dictionaries.

from random import randint

# Create a dictionary to store the history of choices made by the player and the computer.
history_of_choices = {}
# Initialise player and computer score to 0
player_score, comp_score = 0, 0
# Create a list of items which are used in the game.
game_items = ['Rock', 'Paper', 'Scissors']
# Create a winning dictionary
winning_dict = {'Rock': 'Scissors', 'Scissors': 'Paper', 'Paper': 'Rock'}

# Create a loop to run 10 rounds
for round_no in range(1, 11):
    print(f'Welcome to round {round_no}')
    # Input the user choice and create a random choice for computer
    player_choice = input('Enter your move :')
    comp_choice = game_items[randint(0, 2)]
    print(f'Computer choice :{comp_choice}')
    # Add the choices to the respective dictionaries
    history_of_choices[str(round_no)] = [player_choice, comp_choice]
    # Find the winner of the round using winning dictionary
    if player_choice == comp_choice:
        print('It is a tie')
        continue
    if winning_dict[player_choice] == comp_choice:
        player_score += 1  # Increment player score
        print('You won this round!')
    else:
        comp_score += 1  # Increment computer score
        print('Sorry you lose, better luck next time!')
# Finalise the winner and print the scores of both players
print('Game over! 10 rounds completed!')
if player_score == comp_score:
    print('It is a tie')
# Print who is the winner
if player_score > comp_score:
    print(f'Congrats! You won the game by {player_score - comp_score} points')
else:
    print(f'Sorry, the computer won the game by {comp_score - player_score} points')
# Print the scores of both players
print(f'The total points:\nPlayer : {player_score}\tComputer: {comp_score}')
# Print the history of the players
choice = input('Would you like to check the history of a round (y/n) ?')
if choice == 'y':
    round_to_check = input("Enter round number :")
    print(f'Player choice = {history_of_choices[str(round_to_check)][0]}\nComputer choice = {history_of_choices[str(round_to_check)][1]}')





