class Pet:
    species_list = ['Dog', 'Cat', 'Hamster', 'Horse']
    species = None
    name = ""

    def __init__(self, species=None, name=""):
        self.species = species
        self.name = name
        if self.species not in self.species_list:
            raise ValueError("Please enter the following species: Dog, Cat, Hamster, Horse")

    def __str__(self):
        if self.name == "":
            return f"Species of {self.species},unnamed! "
        else:
            return f"Species of {self.species},named {self.name}! "


class Dog(Pet):
    chases = "Cats"

    def __init__(self, name, chases):
        self.name = name
        self.chases = chases
        Pet.__init__(self, 'Dog', self.name)

    def __str__(self):
        if self.name == "":
            return f"Species of: Dog,unnamed chases {self.chases} "
        else:
            return f"Species of: Dog named {self.name} chases {self.chases}! "


class Cat(Pet):
    hates = "Dogs"

    def __init__(self, name="", hates="Dogs"):
        self.name = name
        self.hates = hates
        Pet.__init__(self, 'Cat', self.name)

    def __str__(self):
        if self.name == "":
            return f"Species of: Cat,unnamed hates {self.hates} "
        else:
            return f"Species of: Cat named {self.name} hates {self.hates}! "


if __name__ == "__main__":
    pet_model = Pet("Cat", "Sally")
    print(pet_model)
    pet_model_2 = Cat("Peggy", "Mice")
    print(pet_model_2)
    pet_model_3 = Dog("Rocky", "Apple")
    print(pet_model_3)
    pet_model_4 = Dog("", "Ants")
    print(pet_model_4)
    pet_model_5 = Cat("", "Flies")
    print(pet_model_5)